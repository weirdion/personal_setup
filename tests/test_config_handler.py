#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import logging
import os
from unittest.mock import patch

import pytest

from personal_setup import config_handler, utility_base, constants, path_handler
from personal_setup.models import Symlink


def test_personal_setup_config_defaults():
    """
    Test for config_handler.PersonalSetupConfig.
    Ensure that when a new object is created, the defaults are correct.
    """
    personal_setup_config = config_handler.PersonalSetupConfig()
    assert isinstance(personal_setup_config.symlinks, list)
    assert len(personal_setup_config.symlinks) == 0


def test_personal_setup_config_is_symlink_in_list_invalid(mock_config):
    """
    Test for config_handler.PersonalSetupConfig.is_symlink_in_list.
    Ensure that when an invalid symlink_target is passed, (-1, None) is returned.
    """
    symlink_target = 'xyz'
    index, symlink_result = mock_config.is_symlink_in_list(symlink_target)
    assert index == -1
    assert not symlink_result


def test_personal_setup_config_is_symlink_in_list_source_path(mock_config):
    """
    Test for config_handler.PersonalSetupConfig.is_symlink_in_list.
    Ensure that when a symlink_target is passed that matches with source, it should not be
    checked and (-1, None) is returned.
    """
    symlink_target = 'e'
    index, symlink_result = mock_config.is_symlink_in_list(symlink_target)
    assert index == -1
    assert not symlink_result


def test_personal_setup_config_is_symlink_in_list_destination_path(mock_config):
    """
    Test for config_handler.PersonalSetupConfig.is_symlink_in_list.
    Ensure that when a symlink_target is passed that matches with destination, the matching index
    and the symlink object associated with it is returned.
    """
    symlink_target = os.path.abspath('d')
    index, symlink_result = mock_config.is_symlink_in_list(symlink_target)
    assert index == 1
    assert isinstance(symlink_result, Symlink)
    assert symlink_result.source == os.path.abspath('c')
    assert symlink_result.destination == symlink_target


def test_read_config_from_file_not_found():
    """
    Test for config_handler.read_config_from_file.
    Ensure that if the file doesn't exist, FileNotFoundError is raised
    """
    file_path = utility_base.get_path(path_handler.get_config_file_path())

    # Ensure that if the config exists, it's removed before the test starts
    if os.path.exists(file_path):
        os.remove(file_path)

    with pytest.raises(FileNotFoundError, match=rf'.*{file_path}.*'):
        config_handler.read_config_from_file(file_path)


def test_read_config_from_file_parsing_error(mock_config):
    """
    Test for config_handler.read_config_from_file.
    Ensure that if the file exists, but parsing throws error, TypeError is raised
    """
    file_path = utility_base.get_path(path_handler.get_config_file_path())
    config_handler.write_config_to_file(mock_config, file_path)

    with patch('personal_setup.config_handler._parse_personal_setup_config_dict',
               side_effect=TypeError):
        with pytest.raises(TypeError):
            config_handler.read_config_from_file(file_path)


def test_read_config_from_file_default(mock_config):
    """
    Test for config_handler.read_config_from_file.
    Ensure that if no file_name is specified, path_handler.get_config_file_path() is used.
    Ensure that file is read with correct contents.
    """
    file_path = utility_base.get_path(path_handler.get_config_file_path())
    config_handler.write_config_to_file(mock_config, file_path)

    read_personal_setup_config = config_handler.read_config_from_file(file_path)

    assert len(mock_config.symlinks) == len(read_personal_setup_config.symlinks)

    for expected, actual in zip(mock_config.symlinks,
                                read_personal_setup_config.symlinks):
        assert expected.source == actual.source
        assert expected.destination == actual.destination


def test_read_config_from_file_custom_file(mock_config):
    """
    Test for config_handler.read_config_from_file.
    Ensure that if file_name is specified.
    Ensure that file is read with correct contents.
    """
    file_name = os.path.join(path_handler.get_config_dir(), 'test.json')
    config_handler.write_config_to_file(mock_config, file_name=file_name)

    read_personal_setup_config = config_handler.read_config_from_file(file_name=file_name)

    assert len(mock_config.symlinks) == len(read_personal_setup_config.symlinks)

    for expected, actual in zip(mock_config.symlinks,
                                read_personal_setup_config.symlinks):
        assert expected.source == actual.source
        assert expected.destination == actual.destination


def test_get_current_personal_setup_config_not_found(caplog, check_logs_have_lvl):
    """
    Test for config_handler.get_current_personal_setup_config.
    Ensure that if the file doesn't exist, FileNotFoundError is raised and caught.
    Warning should be logged. Returned object should be empty PersonalSetupConfig.
    """
    file_path = utility_base.get_path(path_handler.get_config_file_path())

    # Ensure that if the config exists, it's removed before the test starts
    if os.path.exists(file_path):
        os.remove(file_path)

    expected_personal_setup_config = config_handler.get_current_personal_setup_config(file_path)
    assert len(expected_personal_setup_config.symlinks) == 0
    check_logs_have_lvl(caplog.records, logging.WARNING, file_path)


def test_get_current_personal_setup_config_parsing_error(caplog, check_logs_have_lvl):
    """
    Test for config_handler.get_current_personal_setup_config.
    Ensure that if the file exists, but parsing throws error, TypeError is raised and caught.
    Error should be logged. Returned object should be empty PersonalSetupConfig.
    """
    file_path = utility_base.get_path(path_handler.get_config_file_path())
    # Ensure that the config exists, write un-formatted text to file_name to force TypeError
    with open(file_path, 'w') as f:
        f.write('test')

    expected_personal_setup_config = config_handler.get_current_personal_setup_config(file_path)
    assert len(expected_personal_setup_config.symlinks) == 0
    check_logs_have_lvl(caplog.records, logging.ERROR, file_path)


def test_get_current_personal_setup_config_default(mock_config):
    """
    Test for config_handler.get_current_personal_setup_config.
    Ensure that if no file_name is specified, path_handler.get_config_file_path() is used.
    Ensure that file is read with correct contents.
    """
    file_path = utility_base.get_path(path_handler.get_config_file_path())
    config_handler.write_config_to_file(mock_config, file_path)

    read_personal_setup_config = config_handler.get_current_personal_setup_config()

    assert len(mock_config.symlinks) == len(read_personal_setup_config.symlinks)

    for expected, actual in zip(mock_config.symlinks,
                                read_personal_setup_config.symlinks):
        assert expected.source == actual.source
        assert expected.destination == actual.destination


def test_write_config_to_file_error(mock_config, caplog, check_logs_have_lvl):
    """
    Test for config_handler.write_config_to_file.
    Ensure that if IOError is encountered, False is returned.
    Error is logged
    """
    err_msg = 'Oops!'
    with patch('builtins.open', side_effect=IOError(err_msg)):
        write_success = config_handler.write_config_to_file(mock_config)

        assert not write_success
        check_logs_have_lvl(caplog.records, logging.ERROR, err_msg)


def test_write_config_to_file_default(mock_config):
    """
    Test for config_handler.write_config_to_file.
    Ensure that if no file_name is specified, path_handler.get_config_file_path() is used.
    Ensure that file is written with correct contents
    """
    file_name = utility_base.get_path(path_handler.get_config_file_path())

    # Ensure that if the config exists, it's removed before the test starts
    if os.path.exists(file_name):
        os.remove(file_name)

    write_success = config_handler.write_config_to_file(mock_config)

    assert write_success
    assert os.path.isfile(file_name)

    read_personal_setup_config = config_handler.read_config_from_file(file_name)

    assert len(mock_config.symlinks) == len(read_personal_setup_config.symlinks)

    for expected, actual in zip(mock_config.symlinks,
                                read_personal_setup_config.symlinks):
        assert expected.source == actual.source
        assert expected.destination == actual.destination


def test_write_config_to_file_custom_file(mock_config):
    """
    Test for config_handler.write_config_to_file.
    Ensure that if file_name is specified.
    Ensure that file is written with correct contents
    """
    file_path = file_name = os.path.join(path_handler.get_config_dir(), 'test.json')

    # Ensure that if the config exists, it's removed before the test starts
    if os.path.exists(file_path):
        os.remove(file_path)

    write_success = config_handler.write_config_to_file(mock_config, file_name=file_name)

    assert write_success
    assert os.path.isfile(file_path)

    read_personal_setup_config = config_handler.read_config_from_file(file_name=file_name)

    assert len(mock_config.symlinks) == len(read_personal_setup_config.symlinks)

    for expected, actual in zip(mock_config.symlinks,
                                read_personal_setup_config.symlinks):
        assert expected.source == actual.source
        assert expected.destination == actual.destination


def test_write_config_to_file_already_exists(mock_config):
    """
    Test for config_handler.write_config_to_file.
    Ensure that if no file_name is specified, path_handler.get_config_file_path() is used.
    If the file already exists, the file should be overwritten.
    Ensure that file is written with correct contents
    """
    file_name = utility_base.get_path(path_handler.get_config_file_path())
    backup_file_name = file_name + constants.CONFIG_BACKUP_SUFFIX

    # Ensure that the config exists, write un-formatted text to file_name
    with open(file_name, 'w') as f:
        f.write('test')
    # Ensure that if the file exists, it's removed before the test starts
    if os.path.exists(backup_file_name):
        os.remove(backup_file_name)

    assert os.path.isfile(file_name)
    assert not os.path.isfile(backup_file_name)

    write_success = config_handler.write_config_to_file(mock_config)

    assert write_success
    assert os.path.isfile(file_name)
    assert os.path.isfile(backup_file_name)

    read_personal_setup_config = config_handler.read_config_from_file(file_name)

    assert len(mock_config.symlinks) == len(read_personal_setup_config.symlinks)

    for expected, actual in zip(mock_config.symlinks,
                                read_personal_setup_config.symlinks):
        assert expected.source == actual.source
        assert expected.destination == actual.destination


def test_parse_personal_setup_config_dict_valid_symlink(mock_config, caplog,
                                                        check_logs_not_warn_error):
    """
    Test for config_handler._parse_personal_setup_config_dict.
    Ensure that if a valid config dict is passed, PersonalSetupConfig object returned has all the
    contents.
    """
    config_dict = json.loads(utility_base.convert_to_json_str(mock_config))
    result = config_handler._parse_personal_setup_config_dict(config_dict)

    assert len(mock_config.symlinks) == len(result.symlinks)

    for expected, actual in zip(mock_config.symlinks,
                                result.symlinks):
        assert expected.source == actual.source
        assert expected.destination == actual.destination

    check_logs_not_warn_error(caplog.records)


def test_parse_personal_setup_config_dict_symlink_no_source(caplog, check_logs_have_lvl):
    """
    Test for config_handler._parse_personal_setup_config_dict.
    Ensure that if an invalid config dict is passed, with no 'source', empty PersonalSetupConfig
    is returned, error is logged
    """
    caplog.clear()
    config_dict = {'symlinks': [{'not_source': 'a', 'destination': 'b'}]}
    result = config_handler._parse_personal_setup_config_dict(config_dict)

    assert len(result.symlinks) == 0

    check_logs_have_lvl(caplog.records, logging.ERROR)


def test_parse_personal_setup_config_dict_symlink_no_destination(caplog, check_logs_have_lvl):
    """
    Test for config_handler._parse_personal_setup_config_dict.
    Ensure that if an invalid config dict is passed, with no 'destination',
    empty PersonalSetupConfig is returned, error is logged
    """
    caplog.clear()
    config_dict = {'symlinks': [{'source': 'a', 'no_destination': 'b'}]}
    result = config_handler._parse_personal_setup_config_dict(config_dict)

    assert len(result.symlinks) == 0

    check_logs_have_lvl(caplog.handler.records, logging.ERROR)


def test_parse_personal_setup_config_dict_symlink_invalid_length(caplog, check_logs_have_lvl):
    """
    Test for config_handler._parse_personal_setup_config_dict.
    Ensure that if a config dict is passed, if any entry that is not 'source', 'destination',
    that entry is skipped and PersonalSetupConfig is returned with valid options.
    Warning is logged
    """
    config_dict = {
        'symlinks': [
            {'source': 'a', 'destination': 'b'},
            {'source': 'c', 'destination': 'd', 'something': 'else'},
            {'source': 'e', 'destination': 'f'},
            {'source': 'g', 'destination': 'h', 'something': 'else'},
        ]}
    result = config_handler._parse_personal_setup_config_dict(config_dict)

    assert len(result.symlinks) == 2
    assert result.symlinks[0].source == 'a'
    assert result.symlinks[0].destination == 'b'
    assert result.symlinks[1].source == 'e'
    assert result.symlinks[1].destination == 'f'

    check_logs_have_lvl(caplog.records, logging.WARNING)


def test_parse_personal_setup_config_dict_no_symlink(caplog, check_logs_not_warn_error):
    """
    Test for config_handler._parse_personal_setup_config_dict.
    Ensure that if a valid config dict is passed with no symlinks, PersonalSetupConfig object
    returned without symlinks.
    """
    config_dict = {
        'test': [
            {'source': 'a', 'destination': 'b'},
            {'source': 'c', 'destination': 'd', 'something': 'else'},
            {'source': 'e', 'destination': 'f'},
            {'source': 'g', 'destination': 'h', 'something': 'else'},
        ]}
    result = config_handler._parse_personal_setup_config_dict(config_dict)

    assert len(result.symlinks) == 0

    check_logs_not_warn_error(caplog.records)
