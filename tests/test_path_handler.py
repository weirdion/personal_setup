#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import sys
from unittest.mock import patch

from personal_setup import constants, path_handler


def test_get_containment_type_no_containment():
    """
    Test for path_handler._get_containment_type.
    Ensure that if there is no containment, ContainmentType.NONE is returned.
    """
    with patch('os.getenv') as os_getenv:
        os_getenv.side_effect = [
            None,  # for pipenv check
            None  # for snapcraft check
        ]

        containment_type = path_handler._get_containment_type()

        assert containment_type is path_handler.ContainmentType.NONE


def test_get_containment_type_snap():
    """
    Test for path_handler._get_containment_type.
    Ensure that if there is snapcraft containment, ContainmentType.SNAP is returned.
    """
    with patch('os.getenv') as os_getenv:
        os_getenv.side_effect = [
            None,  # for pipenv check
            '/snap/some/path'  # for snapcraft check
        ]

        containment_type = path_handler._get_containment_type()

        assert containment_type is path_handler.ContainmentType.SNAP


def test_get_containment_type_pipenv():
    """
    Test for path_handler._get_containment_type.
    Ensure that if there is pipenv containment, ContainmentType.PIPENV is returned.
    """
    with patch('os.getenv') as os_getenv:
        os_getenv.side_effect = [
            '/my/pipenv/path',  # for pipenv check
            None  # for snapcraft check
        ]

        containment_type = path_handler._get_containment_type()

        assert containment_type is path_handler.ContainmentType.PIPENV


def test_get_containment_type_pipenv_snap():
    """
    Test for path_handler._get_containment_type.
    Ensure that if there is pipenv and snapcraft containment both exists, ContainmentType.PIPENV is
    returned.
    """
    with patch('os.getenv') as os_getenv:
        os_getenv.side_effect = [
            '/my/pipenv/path',  # for pipenv check
            '/snap/some/path'  # for snapcraft check
        ]

        containment_type = path_handler._get_containment_type()

        assert containment_type is path_handler.ContainmentType.PIPENV


def test_get_log_dir_confinement_type_none():
    """
    Test for path_handler.get_log_dir.
    Ensure that if ContainmentType is NONE, home dir should be used.
    """
    with patch('personal_setup.path_handler._get_containment_type') as get_containment_type:
        with patch('os.path.isdir', return_value=False):
            with patch('os.makedirs') as make_dirs:
                get_containment_type.return_value = path_handler.ContainmentType.NONE

                log_dir = path_handler.get_log_dir()

                assert log_dir == os.path.join(os.path.expanduser('~'), constants.LOG_DIR)
                # Ensure that folder doesn't exist, os.makedirs is called to create them
                make_dirs.assert_called_once_with(log_dir, mode=0o775)


def test_get_log_dir_confinement_type_pipenv():
    """
    Test for path_handler.get_log_dir.
    Ensure that if ContainmentType is PIPENV, sys.prefix should be used.
    """
    with patch('personal_setup.path_handler._get_containment_type') as get_containment_type:
        with patch('os.path.isdir', return_value=False):
            with patch('os.makedirs') as make_dirs:
                get_containment_type.return_value = path_handler.ContainmentType.PIPENV

                log_dir = path_handler.get_log_dir()

                assert log_dir == os.path.join(sys.prefix, constants.LOG_DIR)
                # Ensure that folder doesn't exist, os.makedirs is called to create them
                make_dirs.assert_called_once_with(log_dir, mode=0o775)


def test_get_log_dir_confinement_type_snap():
    """
    Test for path_handler.get_log_dir.
    Ensure that if ContainmentType is SNAP, SNAP_USER_COMMON should be used.
    """
    snap_mock_path = '/snap/personal-setup/x1'
    with patch('personal_setup.path_handler._get_containment_type') as get_containment_type:
        with patch('os.path.isdir', return_value=False):
            with patch('os.makedirs') as make_dirs:
                with patch('os.getenv', return_value=snap_mock_path) as os_get_env:
                    get_containment_type.return_value = path_handler.ContainmentType.SNAP

                    log_dir = path_handler.get_log_dir()

                    os_get_env.assert_called_once_with('SNAP_USER_COMMON')
                    assert log_dir == os.path.join(snap_mock_path, 'logs')
                    # Ensure that folder doesn't exist, os.makedirs is called to create them
                    make_dirs.assert_called_once_with(log_dir, mode=0o775)


def test_get_config_dir_confinement_type_none():
    """
    Test for path_handler.get_config_dir.
    Ensure that if ContainmentType is NONE, home dir should be used.
    """
    with patch('personal_setup.path_handler._get_containment_type') as get_containment_type:
        with patch('os.path.isdir', return_value=False):
            with patch('os.makedirs') as make_dirs:
                get_containment_type.return_value = path_handler.ContainmentType.NONE

                config_dir = path_handler.get_config_dir()

                assert config_dir == os.path.join(os.path.expanduser('~'), constants.CONFIG_DIR)
                # Ensure that folder doesn't exist, os.makedirs is called to create them
                make_dirs.assert_called_once_with(config_dir, mode=0o775)


def test_get_config_dir_confinement_type_pipenv():
    """
    Test for path_handler.get_config_dir.
    Ensure that if ContainmentType is PIPENV, sys.prefix should be used.
    """
    with patch('personal_setup.path_handler._get_containment_type') as get_containment_type:
        with patch('os.path.isdir', return_value=False):
            with patch('os.makedirs') as make_dirs:
                get_containment_type.return_value = path_handler.ContainmentType.PIPENV

                config_dir = path_handler.get_config_dir()

                assert config_dir == os.path.join(sys.prefix, constants.CONFIG_DIR)
                # Ensure that folder doesn't exist, os.makedirs is called to create them
                make_dirs.assert_called_once_with(config_dir, mode=0o775)


def test_get_config_dir_confinement_type_snap():
    """
    Test for path_handler.get_config_dir.
    Ensure that if ContainmentType is SNAP, SNAP_USER_COMMON should be used.
    """
    snap_mock_path = '/snap/personal-setup/x1'
    with patch('personal_setup.path_handler._get_containment_type') as get_containment_type:
        with patch('os.path.isdir', return_value=False):
            with patch('os.makedirs') as make_dirs:
                with patch('os.getenv', return_value=snap_mock_path) as os_get_env:
                    get_containment_type.return_value = path_handler.ContainmentType.SNAP

                    config_dir = path_handler.get_config_dir()

                    os_get_env.assert_called_once_with('SNAP_USER_COMMON')
                    assert config_dir == os.path.join(snap_mock_path, 'configs')
                    # Ensure that folder doesn't exist, os.makedirs is called to create them
                    make_dirs.assert_called_once_with(config_dir, mode=0o775)
