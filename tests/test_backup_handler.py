#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
import os
from random import randrange
from unittest.mock import patch

from personal_setup import backup_handler, config_handler, path_handler
from personal_setup.constants import CLI_COMMAND_BACKUP

from personal_setup.models import BackupSubCommandOptions, BackupCommandOptions


def test_add_item_symlink(parser):
    """
    Test for backup_handler.add_item.
    Ensure that with backup sub-command SYMLINK, _add_item_symlink is called.
    """
    with patch('personal_setup.backup_handler._add_item_symlink') as add_item_symlink:
        parsed_args = parser.parse_args([CLI_COMMAND_BACKUP, BackupCommandOptions.ADD.value,
                                         BackupSubCommandOptions.SYMLINK.value, '/some/file'])

        backup_handler.add_item(parsed_args)
        add_item_symlink.assert_called_once_with(parsed_args.symlink_object)


def test_remove_item_symlink(parser):
    """
    Test for backup_handler.remove_item.
    Ensure that with backup sub-command SYMLINK, _remove_item_symlink is called.
    """
    with patch('personal_setup.backup_handler._remove_item_symlink') as remove_item_symlink:
        parsed_args = parser.parse_args([CLI_COMMAND_BACKUP, BackupCommandOptions.REMOVE.value,
                                         BackupSubCommandOptions.SYMLINK.value])

        backup_handler.remove_item(parsed_args)
        remove_item_symlink.assert_called_once()


def test_list_items_no_list_option():
    """
    Test for backup_handler.list_items.
    Ensure that with list_option as None, _list_all is called.
    """
    with patch('personal_setup.backup_handler._list_all') as list_all:
        backup_handler.list_items(None)

        assert list_all.called


def test_list_items_symlink():
    """
    Test for backup_handler.list_items.
    Ensure that with list_option as SYMLINK, _list_symlinks is called.
    """
    with patch('personal_setup.backup_handler._list_symlinks') as list_symlinks:
        with patch('personal_setup.backup_handler._list_all') as list_all:
            backup_handler.list_items(BackupSubCommandOptions.SYMLINK.value)

            list_symlinks.assert_called_once()
            assert not list_all.called


def test_add_item_symlink_not_valid_symlink():
    """
    Test for backup_handler._add_item_symlink.
    Ensure that if the symlink_object passed in is not a valid symlink, backup list is not updated.
    """
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        with patch('personal_setup.config_handler.write_config_to_file') as write_config_to_file:
            with patch('os.path.islink') as os_islink:
                os_islink.return_value = False

                backup_handler._add_item_symlink('a')

                assert not get_current_personal_setup_config.called
                assert not write_config_to_file.called


def test_add_item_symlink_simple_add(mock_config):
    """
    Test for backup_handler._add_item_symlink.
    Ensure that if the symlink_object passed in is a valid symlink, backup list is updated with a
    new symlink object added.
    """
    mock_source = '/home/user/z'
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        with patch('personal_setup.config_handler.write_config_to_file') as write_config_to_file:
            with patch('os.path.realpath', return_value=mock_source):
                with patch('os.path.islink', return_value=True):
                    get_current_personal_setup_config.return_value = mock_config

                    initial_length_of_mock = len(mock_config.symlinks)

                    backup_handler._add_item_symlink('z')

                    assert len(mock_config.symlinks) == initial_length_of_mock + 1
                    assert mock_config.symlinks[-1].source == mock_source
                    assert mock_config.symlinks[-1].destination == os.path.abspath('z')
                    write_config_to_file.called_once_with(mock_config,
                                                          path_handler.get_config_file_path())


def test_add_item_symlink_conflict_respond_no(mock_config):
    """
    Test for backup_handler._add_item_symlink.
    Ensure that if the symlink_object passed in is a valid symlink, but the symlink is already
    present in the backup list, user should be prompted.
    If the user responds No, backup list is not updated.
    """
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        with patch('personal_setup.config_handler.write_config_to_file') as write_config_to_file:
            with patch('os.path.islink', return_value=True):
                with patch('personal_setup.utility_base.prompt_user_yes_no', return_value=False) \
                        as prompt_user_yes_no:
                    get_current_personal_setup_config.return_value = mock_config

                    backup_handler._add_item_symlink('b')

                    assert prompt_user_yes_no.called
                    assert not write_config_to_file.called


def test_add_item_symlink_conflict_respond_yes(mock_config):
    """
    Test for backup_handler._add_item_symlink.
    Ensure that if the symlink_object passed in is a valid symlink, but the symlink is already
    present in the backup list, user should be prompted.
    If the user responds Yes, backup list is updated and symlink object replaced.
    """
    new_mock_source = os.path.abspath('x')
    mock_destination = os.path.abspath('f')
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        with patch('personal_setup.config_handler.write_config_to_file') as write_config_to_file:
            with patch('os.path.realpath', return_value=new_mock_source):
                with patch('os.path.islink', return_value=True):
                    with patch('personal_setup.utility_base.prompt_user_yes_no',
                               return_value=True) as prompt_user_yes_no:
                        get_current_personal_setup_config.return_value = mock_config

                        initial_length_of_mock = len(mock_config.symlinks)
                        index_of_symlink, existing_symlink = mock_config.is_symlink_in_list(
                            mock_destination)

                        backup_handler._add_item_symlink('f')

                        assert prompt_user_yes_no.called
                        assert len(mock_config.symlinks) == initial_length_of_mock
                        assert mock_config.symlinks[index_of_symlink].source == new_mock_source
                        assert mock_config.symlinks[
                                   index_of_symlink].destination == mock_destination
                        write_config_to_file.called_once_with(mock_config,
                                                              path_handler.get_config_file_path())


def test_remove_item_symlink_no_symlinks_in_config(caplog, check_logs_have_lvl):
    """
    Test for backup_handler._remove_item_symlink.
    Ensure that if there no symlinks in the config, the loop breaks on it's own.
    """
    caplog.clear()
    personal_setup_config = config_handler.PersonalSetupConfig()
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        with patch('personal_setup.config_handler.write_config_to_file') as write_config_to_file:
            with patch("builtins.input") as mock_input:
                get_current_personal_setup_config.return_value = personal_setup_config

                assert len(personal_setup_config.symlinks) == 0
                backup_handler._remove_item_symlink()

                check_logs_have_lvl(caplog.records, logging.INFO, 'no symlinks')
                assert not mock_input.called
                assert not write_config_to_file.called


def test_remove_item_symlink_remove_invalid_input(mock_config):
    """
    Test for backup_handler._remove_item_symlink.
    Ensure that if there symlinks in the config, and the user provides invalid input option to be
    removed, invalid input is is notified to the user and the quest is re-asked.
    """
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        with patch('personal_setup.config_handler.write_config_to_file') as write_config_to_file:
            with patch("builtins.input") as mock_input:
                invalid_index = len(mock_config.symlinks) + 1
                respond_list = [
                    'y', 'yes', 'no', 'nah', f'{invalid_index}', '-1',  # invalid inputs to re-loop
                    '0'  # valid input to exit
                ]
                mock_input.side_effect = respond_list
                get_current_personal_setup_config.return_value = mock_config

                backup_handler._remove_item_symlink()

                assert not write_config_to_file.called


def test_remove_item_symlink_remove_item_but_not_confirm(mock_config):
    """
    Test for backup_handler._remove_item_symlink.
    Ensure that if there symlinks in the config, and the user provides valid index input option
    to be removed, but if the user says to confirmation, config should not be updated.
    """
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        with patch('personal_setup.config_handler.write_config_to_file') as write_config_to_file:
            with patch("builtins.input") as mock_input:
                valid_index = randrange(1, len(mock_config.symlinks))
                respond_list = [
                    f'{valid_index}',  # valid input to remove symlink
                    'n',  # user confirmation denied
                    '0'  # exit loop
                ]
                mock_input.side_effect = respond_list
                get_current_personal_setup_config.return_value = mock_config

                backup_handler._remove_item_symlink()

                assert not write_config_to_file.called


def test_remove_item_symlink_remove_item_affirmative_confirm(mock_config):
    """
    Test for backup_handler._remove_item_symlink.
    Ensure that if there symlinks in the config, and the user provides valid index input option
    to be removed. If the user says yes to confirmation, config should be updated and saved.
    """
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        with patch('personal_setup.config_handler.write_config_to_file') as write_config_to_file:
            with patch("builtins.input") as mock_input:
                initial_length = len(mock_config.symlinks)
                valid_index = randrange(1, initial_length)
                symlink_to_be_removed = mock_config.symlinks[valid_index - 1]
                respond_list = [
                    f'{valid_index}',  # valid input to remove symlink
                    'y',  # user confirmation affirmed
                    '0'  # exit loop
                ]
                mock_input.side_effect = respond_list
                get_current_personal_setup_config.return_value = mock_config

                backup_handler._remove_item_symlink()

                assert len(mock_config.symlinks) == initial_length - 1
                index, symlink_object = mock_config.is_symlink_in_list(
                    symlink_to_be_removed.destination)
                assert index == -1
                assert not symlink_object
                assert write_config_to_file.called


def test_list_symlinks(mock_config, caplog, check_logs_have_lvl):
    """
    Test for backup_handler._list_symlinks.
    Ensure that all the items from mock_config are printed.
    """
    caplog.clear()
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        get_current_personal_setup_config.return_value = mock_config

        backup_handler._list_symlinks()

        for symlink in mock_config.symlinks:
            check_logs_have_lvl(caplog.records, logging.INFO, symlink.source)
            check_logs_have_lvl(caplog.records, logging.INFO, symlink.destination)


def test_list_all(mock_config):
    """
    Test for backup_handler._list_all.
    Ensure that all functions that print from backup list are called.
    """
    with patch('personal_setup.config_handler.get_current_personal_setup_config') as \
            get_current_personal_setup_config:
        with patch('personal_setup.backup_handler._list_symlinks_from_config') as \
                list_symlinks_from_config:
            get_current_personal_setup_config.return_value = mock_config

            backup_handler._list_all()

            list_symlinks_from_config.assert_called_once_with(mock_config)
