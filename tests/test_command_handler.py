#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from unittest.mock import patch
import pytest

from personal_setup import command_handler
from personal_setup.constants import CLI_COMMAND_BACKUP, CLI_COMMAND_RESTORE
from personal_setup.models import BackupCommandOptions, BackupSubCommandOptions, \
    RestoreCommandOptions


@pytest.mark.parametrize('sub_command_value', BackupSubCommandOptions.member_values())
def test_parse_received_command_backup_add(parser, sub_command_value):
    """
    Test for command_handler.parse_received_command.
    Ensure that if backup command is received with add backup_option.
    """
    parsed_args = parser.parse_args([CLI_COMMAND_BACKUP, BackupCommandOptions.ADD.value,
                                     sub_command_value,
                                     '/some/file'])
    with patch('personal_setup.backup_handler.add_item') as add_item:
        with patch('personal_setup.backup_handler.remove_item') as remove_item:
            with patch('personal_setup.backup_handler.list_items') as list_items:
                with patch('personal_setup.backup_handler.run_backup') as run_backup:
                    command_handler.parse_received_command(parsed_args)

                    assert add_item.called
                    assert not remove_item.called
                    assert not list_items.called
                    assert not run_backup.called


@pytest.mark.parametrize('sub_command_value', BackupSubCommandOptions.member_values())
def test_parse_received_command_backup_remove(parser, sub_command_value):
    """
    Test for command_handler.parse_received_command.
    Ensure that if backup command is received with remove backup_option.
    """
    parsed_args = parser.parse_args([CLI_COMMAND_BACKUP, BackupCommandOptions.REMOVE.value,
                                     sub_command_value])
    with patch('personal_setup.backup_handler.add_item') as add_item:
        with patch('personal_setup.backup_handler.remove_item') as remove_item:
            with patch('personal_setup.backup_handler.list_items') as list_items:
                with patch('personal_setup.backup_handler.run_backup') as run_backup:
                    command_handler.parse_received_command(parsed_args)

                    assert not add_item.called
                    assert remove_item.called
                    assert not list_items.called
                    assert not run_backup.called


@pytest.mark.parametrize('sub_command_value', BackupSubCommandOptions.member_values())
def test_parse_received_command_backup_list(parser, sub_command_value):
    """
    Test for command_handler.parse_received_command.
    Ensure that if backup command is received with list backup_option.
    """
    parsed_args = parser.parse_args([CLI_COMMAND_BACKUP, BackupCommandOptions.LIST.value,
                                     sub_command_value])
    with patch('personal_setup.backup_handler.add_item') as add_item:
        with patch('personal_setup.backup_handler.remove_item') as remove_item:
            with patch('personal_setup.backup_handler.list_items') as list_items:
                with patch('personal_setup.backup_handler.run_backup') as run_backup:
                    command_handler.parse_received_command(parsed_args)

                    assert not add_item.called
                    assert not remove_item.called
                    assert list_items.called
                    assert not run_backup.called


@pytest.mark.parametrize('sub_command_value', BackupSubCommandOptions.member_values())
def test_parse_received_command_backup_run(parser, sub_command_value):
    """
    Test for command_handler.parse_received_command.
    Ensure that if backup command is received with run backup_option.
    """
    parsed_args = parser.parse_args([CLI_COMMAND_BACKUP, BackupCommandOptions.RUN.value,
                                     sub_command_value])
    with patch('personal_setup.backup_handler.add_item') as add_item:
        with patch('personal_setup.backup_handler.remove_item') as remove_item:
            with patch('personal_setup.backup_handler.list_items') as list_items:
                with patch('personal_setup.backup_handler.run_backup') as run_backup:
                    command_handler.parse_received_command(parsed_args)

                    assert not add_item.called
                    assert not remove_item.called
                    assert not list_items.called
                    assert run_backup.called


def test_parse_received_command_restore_symlink(parser):
    """
    Test for command_handler.parse_received_command.
    Ensure that if restore command is received with symlink restore_option.
    """
    parsed_args = parser.parse_args([CLI_COMMAND_RESTORE, RestoreCommandOptions.SYMLINK.value])
    with patch('personal_setup.restore_handler.restore_symlinks') as restore_symlinks:
        with patch('personal_setup.restore_handler.restore_all') as restore_all:
            command_handler.parse_received_command(parsed_args)

            assert restore_symlinks.called
            assert not restore_all.called


def test_parse_received_command_restore_all(parser):
    """
    Test for command_handler.parse_received_command.
    Ensure that if restore command is received with all restore_option.
    """
    parsed_args = parser.parse_args([CLI_COMMAND_RESTORE, RestoreCommandOptions.ALL.value])
    with patch('personal_setup.restore_handler.restore_symlinks') as restore_symlinks:
        with patch('personal_setup.restore_handler.restore_all') as restore_all:
            command_handler.parse_received_command(parsed_args)

            assert not restore_symlinks.called
            assert restore_all.called
