#  personal-setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal-setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal-setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
import sys
from unittest.mock import patch

import pytest

from personal_setup import constants, cli


def test_create_parser_version(parser, capfd):
    """
    Test for cli.create_parser.
    Ensure that the version flag prints the correct version from constants.
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['--version'])

        out, err = capfd.readouterr()

        assert constants.__version__ in out
        assert not err


def test_create_parser_help(parser, capfd):
    """
    Test for cli.create_parser and cli._HelpActionLongFormat
    Ensure that parser '-h' flag prints long help
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['--help'])

        out, err = capfd.readouterr()

        assert "Command 'backup'" in out
        assert "Command 'restore'" in out


def test_create_parser_backup_no_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup` with no argument results in SystemExit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['backup'])


def test_create_parser_backup_invalid_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup test` as an invalid argument results in SystemExit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['backup', 'test'])


def test_create_parser_backup_add_no_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup add` with no argument results in SystemExit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['backup', 'add'])


def test_create_parser_backup_add_symlink_without_object(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup add symlink` with no argument results in SystemExit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['backup', 'add', 'symlink'])


def test_create_parser_backup_add_symlink(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup add symlink file` has correct values assigned
    """
    symlink_file = 'my-symlink'
    args = parser.parse_args(['backup', 'add', 'symlink', symlink_file])

    assert args.command == 'backup'
    assert args.backup_option == 'add'
    assert args.add_option == 'symlink'
    assert args.symlink_object == symlink_file


def test_create_parser_backup_remove_no_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup remove` with no argument results in SystemExit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['backup', 'remove'])


def test_create_parser_backup_remove_symlink_without_object(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup remove symlink` with no SystemExit is raised
    """
    args = parser.parse_args(['backup', 'remove', 'symlink'])

    assert args.command == 'backup'
    assert args.backup_option == 'remove'
    assert args.remove_option == 'symlink'


def test_create_parser_backup_list_no_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup list` with no argument does not result in SystemExit
    command and backup_option are correctly assigned.
    """
    args = parser.parse_args(['backup', 'list'])

    assert args.command == 'backup'
    assert args.backup_option == 'list'


def test_create_parser_backup_list_symlink(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup list symlink` has correct values assigned
    """
    args = parser.parse_args(['backup', 'list', 'symlink'])

    assert args.command == 'backup'
    assert args.backup_option == 'list'
    assert args.list_option == 'symlink'


def test_create_parser_backup_run_no_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup run` with no argument does not result in SystemExit
    command and backup_option are correctly assigned.
    """
    args = parser.parse_args(['backup', 'run'])

    assert args.command == 'backup'
    assert args.backup_option == 'run'


def test_create_parser_backup_run_symlink(parser):
    """
    Test for cli.create_parser
    Ensure that calling `backup run symlink` has correct values assigned
    """
    args = parser.parse_args(['backup', 'run', 'symlink'])

    assert args.command == 'backup'
    assert args.backup_option == 'run'
    assert args.run_option == 'symlink'


def test_create_parser_restore_no_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `restore` with no argument results in SystemExit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['restore'])


def test_create_parser_restore_invalid_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `restore test` as an invalid argument results in SystemExit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['restore', 'test'])


def test_create_parser_restore_all(parser):
    """
    Test for cli.create_parser
    Ensure that calling `restore all` has correct values assigned
    """
    args = parser.parse_args(['restore', 'all'])

    assert args.command == 'restore'
    assert args.restore_option == 'all'


def test_create_parser_restore_all_invalid_sub_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `restore all test` as an invalid argument raises SystemExit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['restore', 'all', 'test'])


def test_create_parser_restore_symlink(parser):
    """
    Test for cli.create_parser
    Ensure that calling `restore symlink` has correct values assigned
    """
    args = parser.parse_args(['restore', 'symlink'])

    assert args.command == 'restore'
    assert args.restore_option == 'symlink'


def test_create_parser_restore_symlink_invalid_sub_argument(parser):
    """
    Test for cli.create_parser
    Ensure that calling `restore symlink test` as an invalid argument raises SystemExit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(['restore', 'symlink', 'test'])


def test_main_no_argument_received(capfd):
    """
    Test for cli.main.
    Ensure that if cli is called without any arguments, SystemExit is triggered with an error.
    """
    with patch("sys.argv", return_value=[]):
        with pytest.raises(SystemExit):
            cli.main()


def test_main_argument_received(capfd):
    """
    Test for cli.main.
    Ensure that if cli is called with any arguments, control is passed to command_handler.
    """
    with patch("personal_setup.command_handler.parse_received_command") as parse_received_command:
        with patch.object(sys, "argv", ['personal_setup', 'restore', 'all']):
            cli.main()
            assert parse_received_command.call_args
