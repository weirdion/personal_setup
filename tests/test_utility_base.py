#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import re
from enum import auto
import sys

from unittest.mock import patch

from personal_setup import utility_base
import pytest


def test_base_enum_generate_next_value():
    """
    Test for utility_base.BaseEnum._generate_next_value_
    Ensure that child class generated with base enum has value as lower version of the name.
    """

    class TestEnum(utility_base.BaseEnum):
        TEST_1 = auto()
        TEST2 = auto()

    assert TestEnum.TEST_1.value == 'test_1'
    assert TestEnum.TEST2.value == 'test2'


def test_base_enum_member_names():
    """
    Test for utility_base.BaseEnum.member_names.
    Ensure that member names are returned as a list.
    """

    class TestEnum(utility_base.BaseEnum):
        TEST_1 = auto()
        TEST2 = auto()

    assert TestEnum.member_names() == [TestEnum.TEST_1.name, TestEnum.TEST2.name]


def test_base_enum_member_values():
    """
    Test for utility_base.BaseEnum.member_values.
    Ensure that member values are returned as a list.
    """

    class TestEnum(utility_base.BaseEnum):
        TEST_1 = auto()
        TEST2 = auto()

    assert TestEnum.member_values() == [TestEnum.TEST_1.value, TestEnum.TEST2.value]


def test_base_enum_check_if_member():
    """
    Test for utility_base.BaseEnum.check_if_member.
    Ensure that member name (case in-sensitive) returns True.
    Returns False if invalid member name is used.
    """

    class TestEnum(utility_base.BaseEnum):
        TEST_1 = auto()
        TEST2 = auto()

    assert TestEnum.check_if_member(TestEnum.TEST_1.name)
    assert TestEnum.check_if_member(TestEnum.TEST2.value)
    assert TestEnum.check_if_member('tEsT_1')
    assert TestEnum.check_if_member('TesT2')

    assert not TestEnum.check_if_member('test')
    assert not TestEnum.check_if_member('TesT3')


def test_prompt_user_yes_no_invalid_reply(capfd):
    """
    Test for utility_base.prompt_user_yes_no.
    Ensure that if the user puts in invalid input, the input keeps prompting.
    """
    with patch("builtins.input") as input:
        respond_list = ['ya', 'yo', 'Nah', 'Nope', 'abcd', 'test', 'y']
        input.side_effect = respond_list

        utility_base.prompt_user_yes_no('Do you even test?')

        out, err = capfd.readouterr()

        # Ensure that "Invalid input" appears same as length of respond_list -1
        # -1, since last respond is valid to break while loop
        assert len(re.findall(r'Invalid input', out)) == len(respond_list) - 1


@pytest.mark.parametrize(('user_input', 'expected_bool'), [
    ('y', True), ('Y', True), ('YeS', True), ('YES', True),
    ('n', False), ('N', False), ('No', False), ('NO', False)
])
def test_prompt_user_yes_no_valid_reply(capfd, user_input, expected_bool):
    """
    Test for utility_base.prompt_user_yes_no.
    Ensure that if the user inputs valid input, the function returns True/False.
    """
    with patch("builtins.input") as input:
        input.side_effect = user_input

        result = utility_base.prompt_user_yes_no('Do you even test?')

        assert result == expected_bool

        out, err = capfd.readouterr()

        assert not out
        assert not err


def test_get_os_prefix_contained():
    """
    Test for utility_base.get_os_prefix.
    Ensure that if the pipenv is active, contained prefix is returned.
    """
    prefix = '/home/user/my/pipenv'
    with patch('personal_setup.utility_base._is_pipenv_active', return_value=True):
        with patch.object(sys, "prefix", prefix):
            result = utility_base.get_os_prefix()

            assert result == prefix + '/'


def test_get_os_prefix_not_contained():
    """
    Test for utility_base.get_os_prefix.
    Ensure that if the pipenv is not active, '/' is returned.
    """
    prefix = '/home/user/my/pipenv'
    with patch('personal_setup.utility_base._is_pipenv_active', return_value=False):
        with patch.object(sys, "prefix", prefix):
            result = utility_base.get_os_prefix()

            assert prefix not in result
            assert result == '/'


def test_get_path_tilde_path_w_prefix_contained():
    """
    Test for utility_base.get_path.
    Ensure tht if a path with '~' is passed as arg, when contained.
    """
    prefix = '/home/user/my/pipenv'
    path = '.config/myapp'
    file_path = f'~/{path}'
    home_path = os.path.expanduser('~')
    with patch('personal_setup.utility_base.get_os_prefix', return_value=prefix):
        result = utility_base.get_path(file_path)
        assert result == f'{prefix}{home_path}/{path}'


def test_get_path_tilde_path_w_prefix_not_contained():
    """
    Test for utility_base.get_path.
    Ensure tht if a path with '~' is passed as arg, when not contained.
    """
    prefix = '/'
    path = '.config/myapp'
    file_path = f'~/{path}'
    home_path = os.path.expanduser('~')
    with patch('personal_setup.utility_base.get_os_prefix', return_value=prefix):
        result = utility_base.get_path(file_path)
        assert result == f'{home_path}/{path}'


def test_get_path_non_absolute_path_w_prefix_contained():
    """
    Test for utility_base.get_path.
    Ensure tht if a path with non absolute path is passed as arg, when contained.
    """
    prefix = '/home/user/my/pipenv'
    file_path = 'var/config/myapp'
    with patch('personal_setup.utility_base.get_os_prefix', return_value=prefix):
        result = utility_base.get_path(file_path)
        assert result == f'{prefix}/{file_path}'


def test_get_path_non_absolute_path_w_prefix_not_contained():
    """
    Test for utility_base.get_path.
    Ensure tht if a path with non absolute path is passed as arg, when not contained.
    """
    prefix = '/'
    file_path = 'var/config/myapp'
    with patch('personal_setup.utility_base.get_os_prefix', return_value=prefix):
        result = utility_base.get_path(file_path)
        assert result == f'{prefix}{file_path}'


def test_get_path_absolute_path_w_prefix_contained():
    """
    Test for utility_base.get_path.
    Ensure tht if a path with absolute path is passed as arg, when contained.
    """
    prefix = '/home/user/my/pipenv'
    file_path = '/var/config/myapp'
    with patch('personal_setup.utility_base.get_os_prefix', return_value=prefix):
        result = utility_base.get_path(file_path)
        assert result == f'{prefix}{file_path}'


def test_get_path_absolute_path_w_prefix_not_contained():
    """
    Test for utility_base.get_path.
    Ensure tht if a path with absolute path is passed as arg, when not contained.
    """
    prefix = '/'
    file_path = '/var/config/myapp'
    with patch('personal_setup.utility_base.get_os_prefix', return_value=prefix):
        result = utility_base.get_path(file_path)
        assert result == file_path


def test_get_path_absolute_path_when_already_full_path():
    """
    Test for utility_base.get_path.
    Ensure tht if a path with absolute path is already complete, return as is.
    """
    prefix = '/home/user/my/pipenv'
    file_path = '/home/user/my/pipenv/var/config/myapp'
    with patch('personal_setup.utility_base.get_os_prefix', return_value=prefix):
        result = utility_base.get_path(file_path)
        assert result == file_path


def test_is_pipenv_active():
    """
    Test for utility_base._is_pipenv_active.
    Ensure that when pipenv is active, True is returned.
    """
    with patch('os.getenv', return_value='/some/path'):
        assert utility_base._is_pipenv_active()


def test_is_pipenv_inactive():
    """
    Test for utility_base._is_pipenv_active.
    Ensure that when pipenv is not active, False is returned.
    """
    with patch('os.getenv', return_value=None):
        assert not utility_base._is_pipenv_active()


def test_convert_to_json_str_for_enum():
    """
    Test for utility_base.convert_to_json_str.
    Ensure that if an enum object is passed, only the value is returned.
    Value should be in lower case and in double-quotes (json-formatting).
    """

    class TestEnum(utility_base.BaseEnum):
        TEST1 = auto()

    result = utility_base.convert_to_json_str(TestEnum.TEST1)
    assert result == '"test1"'


def test_convert_to_json_str_for_class_obj():
    """
    Test for utility_base.convert_to_json_str.
    Ensure that if any class object is passed, it is returned in dict format.
    Values in dict should be in double-quotes (json-formatting).
    """

    class TestClass:
        def __init__(self, var_1, var_2):
            self.var_1 = var_1
            self.var_2 = var_2

    test_class_obj = TestClass("value1", "value2")
    result = utility_base.convert_to_json_str(test_class_obj)
    assert result == '{"var_1": "value1", "var_2": "value2"}'


def test_convert_to_json_str_for_class_obj_with_indent():
    """
    Test for utility_base.convert_to_json_str.
    Ensure that if any class object is passed, it is returned in dict format.
    Values in dict should be in double-quotes (json-formatting).
    The string should be indented with 4 spaces.
    """

    class TestClass:
        def __init__(self, var_1, var_2):
            self.var_1 = var_1
            self.var_2 = var_2

    test_class_obj = TestClass("value1", "value2")
    result = utility_base.convert_to_json_str(test_class_obj, indent=4)
    assert result == '{\n    "var_1": "value1",\n    "var_2": "value2"\n}'
