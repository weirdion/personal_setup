#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
import os
from unittest.mock import patch

import pytest

from personal_setup import restore_handler
from personal_setup.models import Symlink


@pytest.fixture
def mock_symlink():
    return Symlink(os.path.abspath('a'), os.path.abspath('b'))


def test_restore_individual_symlink_none_exists(mock_symlink):
    """
    Test for restore_handler._restore_individual_symlink.
    Ensure that if symlink.destination doesn't exist, create one.
    """
    with patch('personal_setup.utility_system.create_symlink') as create_symlink:
        with patch('os.path.lexists') as os_path_lexists:
            os_path_lexists.return_value = False

            restore_handler._restore_individual_symlink(mock_symlink)

            create_symlink.assert_called_once_with(mock_symlink.source, mock_symlink.destination)


def test_restore_individual_symlink_file_exists_no_error(mock_symlink, caplog, check_logs_have_lvl):
    """
    Test for restore_handler._restore_individual_symlink.
    Ensure that if symlink.destination exist, but is a file, then user should be prompted,
    and then file removed with warning shown.
    """
    with patch('personal_setup.utility_system.create_symlink') as create_symlink:
        with patch('os.path.lexists') as os_path_lexists:
            os_path_lexists.return_value = True
            with patch('os.path.islink') as os_path_is_link:
                os_path_is_link.return_value = False
                with patch('personal_setup.utility_base.prompt_user_yes_no', return_value=True) as \
                        prompt_user:
                    with patch('os.remove') as os_remove:
                        restore_handler._restore_individual_symlink(mock_symlink)

                        prompt_user.assert_called()
                        os_remove.assert_called_once_with(mock_symlink.destination)

                        create_symlink.assert_called_once_with(mock_symlink.source,
                                                               mock_symlink.destination)

                        check_logs_have_lvl(caplog.records, logging.WARNING)


def test_restore_individual_symlink_file_exists_w_error(mock_symlink, caplog, check_logs_have_lvl):
    """
    Test for restore_handler._restore_individual_symlink.
    Ensure that if symlink.destination exist, but is a file, then user should be prompted,
    and but file removal fails, error is printed.
    """
    err_msg = "Oops!"
    with patch('personal_setup.utility_system.create_symlink') as create_symlink:
        with patch('os.path.lexists') as os_path_lexists:
            os_path_lexists.return_value = True
            with patch('os.path.islink') as os_path_is_link:
                os_path_is_link.return_value = False
                with patch('personal_setup.utility_base.prompt_user_yes_no', return_value=True) as \
                        prompt_user:
                    with patch('os.remove', side_effect=OSError(err_msg)) as os_remove:
                        restore_handler._restore_individual_symlink(mock_symlink)

                        prompt_user.assert_called()
                        os_remove.assert_called_once_with(mock_symlink.destination)

                        assert not create_symlink.called

                        check_logs_have_lvl(caplog.records, logging.ERROR, err_msg)


def test_restore_individual_symlink_link_exists_no_error(mock_symlink, caplog, check_logs_have_lvl):
    """
    Test for restore_handler._restore_individual_symlink.
    Ensure that if symlink.destination exist, but is already a link, then user should be prompted,
    and if accepts, then link removed with warning shown.
    """
    with patch('personal_setup.utility_system.create_symlink') as create_symlink:
        with patch('os.path.lexists') as os_path_lexists:
            os_path_lexists.return_value = True
            with patch('os.path.islink') as os_path_is_link:
                os_path_is_link.return_value = True
                with patch('personal_setup.utility_base.prompt_user_yes_no', return_value=True) as \
                        prompt_user:
                    with patch('os.remove') as os_remove:
                        with patch('os.path.realpath', return_value=mock_symlink.destination):
                            restore_handler._restore_individual_symlink(mock_symlink)

                            prompt_user.assert_called()
                            os_remove.assert_called_once_with(mock_symlink.destination)

                            create_symlink.assert_called_once_with(mock_symlink.source,
                                                                   mock_symlink.destination)

                            check_logs_have_lvl(caplog.records, logging.WARNING)


def test_restore_individual_symlink_link_exists_w_error(mock_symlink, caplog, check_logs_have_lvl):
    """
    Test for restore_handler._restore_individual_symlink.
    Ensure that if symlink.destination exist, but is already a link, then user should be prompted,
    and if accepts, but link removal fails, error is shown.
    """
    err_msg = "Oops!"
    with patch('personal_setup.utility_system.create_symlink') as create_symlink:
        with patch('os.path.lexists') as os_path_lexists:
            os_path_lexists.return_value = True
            with patch('os.path.islink') as os_path_is_link:
                os_path_is_link.return_value = True
                with patch('personal_setup.utility_base.prompt_user_yes_no', return_value=True) as \
                        prompt_user:
                    with patch('os.remove', side_effect=OSError(err_msg)) as os_remove:
                        with patch('os.path.realpath', return_value=mock_symlink.destination):
                            restore_handler._restore_individual_symlink(mock_symlink)

                            prompt_user.assert_called()
                            os_remove.assert_called_once_with(mock_symlink.destination)

                            assert not create_symlink.called

                            check_logs_have_lvl(caplog.records, logging.ERROR, err_msg)
