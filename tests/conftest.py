#  personal-setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal-setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal-setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Conftest for PyTest - Helper fixtures
"""
import logging
import os
import re

import pytest
from pytest import fixture

from personal_setup import cli, config_handler
from personal_setup.models import Symlink


@fixture
def parser():
    return cli.create_parser()


@pytest.fixture
def check_logs_le_info():
    def check_logs_do_not_exceed_info(records):
        """
        Helper function that checks the log records if any exceeded INFO.
        :param records: record list from caplog
        """
        for record in records:
            assert record.levelno <= logging.INFO

    return check_logs_do_not_exceed_info


@pytest.fixture
def check_logs_not_warn_error():
    def check_logs_do_not_have_warning_or_error(records):
        """
        Helper function that checks the log records if any exceeded INFO.
        :param records: record list from caplog
        """
        warning_error_not_found = True
        for record in records:
            if record.levelno >= logging.WARNING:
                warning_error_not_found = False
                break

        assert warning_error_not_found

    return check_logs_do_not_have_warning_or_error


@pytest.fixture
def check_logs_have_lvl():
    return check_logs_have_level_message


@pytest.fixture
def mock_config():
    symlink_list = [Symlink(os.path.abspath('a'), os.path.abspath('b')),
                    Symlink(os.path.abspath('c'), os.path.abspath('d')),
                    Symlink(os.path.abspath('e'), os.path.abspath('f')),
                    Symlink(os.path.abspath('g'), os.path.abspath('h'))]

    return config_handler.PersonalSetupConfig(symlinks=symlink_list)


def check_logs_have_level_message(records, log_level, log_msg=None):
    """
    Helper function that parses through log records, searches to ensure log_level exists.
    If log_msg is given, function checks if log_msg appears in log_level.
    :param records: record list from caplog
    :param log_level: logging level to look for
    :param log_msg: error message to check if it was printed
    """
    log_type_found = False
    log_msg_found = False

    for record in records:
        if record.levelno == log_level:
            log_type_found = True
            if log_msg and re.search(log_msg, record.message, re.IGNORECASE):
                log_msg_found = True
                break

    assert log_type_found

    if log_msg:
        assert log_msg_found
