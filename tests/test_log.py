#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging

from personal_setup import log


def test_create_logger_defaults():
    """
    Test for log.create_logger.
    Ensure that the defaults of the logger are correct.
    """
    logger = log.create_logger(__name__)

    assert logger.level == logging.INFO
    assert len(logger.handlers) == 2
    assert logger.handlers[0].level == logging.DEBUG
    assert logger.handlers[1].level == logging.ERROR


def test_console_logger_defaults():
    """
    Test for log.console_logger.
    Ensure that the defaults of the logger are correct.
    """
    logger = log.console_logger()

    assert logger.level == logging.INFO
    assert logger.handlers[0].level == logging.INFO
