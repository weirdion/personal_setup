#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
from unittest.mock import patch

from personal_setup import utility_system


def test_create_symlink_error(caplog, check_logs_have_lvl):
    """
    Test for utility_system.create_symlink.
    Ensure that if OSError is encountered, error is logged, False is returned.
    """
    source_file = '/home/user/myfile'
    dest_file = '/home/user/new-file'
    err_msg = "Oops!"
    with patch("os.symlink", side_effect=OSError(err_msg)):
        result = utility_system.create_symlink(source_file, dest_file)

        check_logs_have_lvl(caplog.records, logging.ERROR, source_file)
        check_logs_have_lvl(caplog.records, logging.ERROR, dest_file)
        assert not result


def test_create_symlink(caplog, check_logs_not_warn_error):
    """
    Test for utility_system.create_symlink.
    Ensure that if no OSError is encountered, True is returned.
    """
    source_file = '/home/user/myfile'
    dest_file = '/home/user/new-file'
    with patch("os.symlink") as symlink:
        result = utility_system.create_symlink(source_file, dest_file)

        symlink.assert_called_with(source_file, dest_file)

        check_logs_not_warn_error(caplog.records)

        assert result
