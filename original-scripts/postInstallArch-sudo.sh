#!/bin/bash

# Script to run as a root user.
# This script mounts the backup partition, installs graphics for bumblebee,
# sets up groups for user and appends fstab for the backup partition.

declare -r scriptUser=$(who | awk 'NR==1{print $1}')
declare -r storeMountDir="/run/media/$scriptUser/store"
declare -r backupDir="$storeMountDir/backup"
declare -r homeDir="/home/$scriptUser"
declare -r storeBlock=/dev/mapper/store
declare -r scriptDir="$(dirname "${BASH_SOURCE[0]}")"

echo "$USER used when running this script"

mountStore() {
	# Check if dir exists or create one
	if [ ! -d "$storeMountDir" ]; then
		echo "Creating directory $storeMountDir"
		mkdir -p $storeMountDir
	else
		echo "Directory already exists"
	fi

	# Mount block onto dir
	if [ -d "$storeMountDir" ]; then
		echo "Attempting to mount $storeBlock onto $storeMountDir"
		mount $storeBlock $storeMountDir
	else 
		echo "Directory $storeMountDir not found, please check manually and try again.."
		exit
	fi

	# Check if the block was mounted successfully
	if mount | grep "$storeBlock" > /dev/null ; then
		echo "$storeBlock was successfully mounted"
	else
		echo "Failed to mount $storeBlock"
		exit
	fi
}

function _bumblebeeSetup() {
	pacman -Rc xf86-video-nouveau
	pacman -S bumblebee mesa nvidia lib32-nvidia-utils lib32-virtualgl nvidia-settings bbswitch
	systemctl enable bumblebeed.service
	gpasswd -a $scriptUser bumblebee
}

function handleGroups() {
	groupadd sdkusers
	gpasswd -a $scriptUser lp
	gpasswd -a $scriptUser sdkusers
	gpasswd -a $scriptUser adm
	gpasswd -a $scriptUser scanner
	gpasswd -a $scriptUser ftp
	gpasswd -a $scriptUser rfkill
	gpasswd -a $scriptUser sys
	gpasswd -a $scriptUser video
}

# Create a swap file at /swapfile
createSwapFile() {
	dd if=/dev/zero of=/swapfile bs=1M count=12288 && \
	chmod 600 /swapfile && \
	mkswap /swapfile && swapon /swapfile
}

# Pulls the backed up lines from fstab-store
appendFstab() {
	echo
	read -r -p "Do you want to add to fstab? [y/n]: " response3
	response3=${response3,,}
	if [[ "$response3" =~ ^(yes|y)$ ]]; then
		echo "Making a backup for fstab"
		cp -v /etc/fstab /etc/fstab.backup
		while read line; do
			echo $line >> /etc/fstab
		done < $backupDir/fstab-store
		if [[ -f /swapfile ]]; then
			echo -e "\n# Swapfile" >> /etc/fstab
			echo "/swapfile none swap defaults 0 0" >> /etc/fstab
		fi
		echo "fstab appending complete"
		echo
		cat /etc/fstab
	fi
}

# Create symbolic links for systemd services and profile.d
createSymLinkToScripts() {
	for service in "$backupDir/services/*.service"; do
		ln -s $service /etc/systemd/system/
	done
	dirName=`dirname $(readlink -f $0)`
	ln -s "$dirName/my-etc/sdk-env-profile.sh" /etc/profile.d/
}

# TODO this needs to be a menu
# Check if script is running as root
if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root"
  exit
else
	mountStore
	handleGroups
	swapfileCreation
	appendFstab
	createSymLinkToScripts
fi
