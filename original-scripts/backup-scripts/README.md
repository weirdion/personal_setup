# Backup-Scripts

---

Bash scripts with cron jobs to do

- Weekly backups to lvm store/backup partition
- Monthly backups to external drive

Bash script for move media files to media server with incron

## Cron for weekly

At 7 AM on Sundays, run script and log output.
 > 00 07 * * 0 /home/asadana/workspace/personal-arch-setup/backup-scripts/weekly-backup.sh >> /home/asadana/weekly-backup.log
