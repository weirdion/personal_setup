#!/bin/bash

# Script to copy all the files from home directory
# and copy them into the store/backup directory.

declare -r scriptUser="$LOGNAME"
declare -r homeDir="/home/$scriptUser"
declare -r backupDir="$homeDir/backup"
declare -r scriptDir=`dirname $(readlink -f $0)`
declare -r installedPackageList="$backupDir/installed-packages.pacman"
declare -r installedPackageAURList="$backupDir/installed-packages.pacman-aur"

# Copy whitelisted files from the home directory to backup destination
function _copyAllFilesInList() {
	echo "Starting to backup all files and configs..."
	rsync -clhr --progress --backup --backup-dir=$backupDir/old \
	--files-from=$scriptDir/weekly-backup-list $homeDir $backupDir
	echo "Finished backuping up all files and configs!"
}

function _backupInstallPackageList() {
	echo "Starting to backup all installed packages..."
	if [ -f "$installedPackageList" ]; then
		echo "Backing up existing list..." && mv "$installedPackageList" "$installedPackageList.backup"
	fi
	if [ -f "$installedPackageAURList" ]; then
		echo "Backing up existing AUR list..." && mv "$installedPackageAURList" "$installedPackageAURList.backup"
	fi
	pacman -Qqe | grep -v "$(pacman -Qqm)" > "$installedPackageList"
	pacman -Qqm > "$installedPackageAURList"
	echo "Finished backuping up all installed packages!"
}

# Check if script is running as root
if [ "$EUID" -eq 0 ]
  then echo -e "\nPlease run this script as a non-root user\n"
  exit
else
	_copyAllFilesInList
	_backupInstallPackageList
fi