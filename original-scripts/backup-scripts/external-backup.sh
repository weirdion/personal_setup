#!/bin/bash

# Script to copy all the files from store directory
# and copy them to external hard drive or provided destination.

declare -r scriptUser="$LOGNAME"
declare -r storeDir="/run/media/$scriptUser/store"
declare -r hddDir="/run/media/$scriptUser/hdd-backup"
declare -r scriptDir=`dirname $(readlink -f $0)`

# Copy all files from the source directory to destination, with exceptions from blacklist
copyAllFilesExceptBlacklist() {
	rsync -chlr --progress --delete-after --exclude-from=$scriptDir/external-backup-blacklist \
	--filter="merge external-backup-exception" $storeDir $1
}

# Check if script is running as root
if [ "$EUID" -eq 0 ]; then
	echo -e "\nPlease run this script as a non-root user\n"
  exit
else
	if [[ $# -lt 1 ]]; then
		echo -e "\nNo argument provided,\nusing default path to External location: $hddDir\n"
		if [[ -d $hddDir ]]; then
			copyAllFilesExceptBlacklist $hddDir
		else
			echo -e "Sorry the destination location doesn't exist: $hddDir.\n"
			exit 1
		fi
	else
		if [[ -d $1 ]]; then
			echo -e "\nArgument provided, setting destination as $1\n"
			copyAllFilesExceptBlacklist $1
		else
			echo -e "\nSorry the destination location doesn't exist: $1.\n"
			exit 1
		fi
	fi
fi
