#!/bin/bash

# Script to run as a non-root user.
# This script creates soft-links, restore backup config files 
# and run yay (pacman AUR wrapper) for remaining applications.

declare -r scriptUser="$LOGNAME"
declare -r storeMountDir="/run/media/$scriptUser/store"
declare -r homeDir="/home/$scriptUser"
declare -r backupDir="$storeMountDir/backup"

createSoftLinks() {
	echo
	read -r -p "Do you want to delete \"Documents\", \"Pictures\" and \"Downloads\"  before creating soft links? [y/n]: " response
	response=${response,,}    # to lower
	if [[ "$response" =~ ^(yes|y)$ ]]; then
		if [[ -d "$homeDir/Documents" ]]; then
			rm -vrf $homeDir/Documents
		fi
		if [[ -d "$homeDir/Pictures" ]]; then
			rm -vrf $homeDir/Pictures
		fi
		if [[ -d "$homeDir/Downloads" ]]; then
			rm -vrf $homeDir/Downloads
		fi
	else
	    exit;
	fi

	echo "Starting to generate soft links"
	ln -s "$storeMountDir" "$homeDir/store" && \
	ln -s "$storeMountDir/Documents" "$homeDir/Documents" && \
	ln -s "$storeMountDir/Downloads" "$homeDir/Downloads" && \
	ln -s "$storeMountDir/Pictures" "$homeDir/Pictures" && \
	ln -s "$storeMountDir/torrents" "$homeDir/torrents" && \
	ln -s "$storeMountDir/wallpapers" "$homeDir/wallpapers" && \
	ln -s "$storeMountDir/workspace" "$homeDir/workspace" && \
	ln -s "$storeMountDir/sdk" "$homeDir/sdk"
	cp -rv "$storeMountDir/.AndroidStudio3.4" "$homeDir/.AndroidStudio3.4"
	cp -rv "$storeMountDir/.IdeaIC2019.1" "$homeDir/.IdeaIC2019.1"

	echo
	echo "Soft links created"
	ls -ahl --color=auto $homeDir
}

copyConfigFilesFromBackup() {
	echo
	read -r -p "Do you want to delete existing bashrc and stuff and restore from backup? [y/n]: " response2
	response2=${response2,,}
	echo
	if [[ "$response2" =~ ^(yes|y)$ ]]; then
		rm -v $homeDir/.bashrc
		rm -v $homeDir/.face
		rm -v $homeDir/.face.icon
		rm -v $homeDir/.gitconfig
		rm -rfv $homeDir/.m2
		rm -rfv $homeDir/.ssh
		cp -v $backupDir/.bashrc $homeDir/
		cp -v $backupDir/.face $homeDir/ && \
		cp -v $backupDir/.gitconfig $homeDir/ && \
		cp -rv $backupDir/.m2 $homeDir/.m2 && \
		cp -rv $backupDir/.ssh $homeDir/.ssh
		ln -s $homeDir/.face $homeDir/.face.icon
	fi
	echo
}

function _installApplicationsWithAurHelper() {
	yay -S --noconfirm jdk8-openjdk vim snapd
}

function _installAppsWithSnap() {
	sudo systemctl enable --now snapd && \
	sudo ln -s /var/lib/snapd/snap /snap
	snapClassicList=("intellij-idea-community" "android-studio")
	snapList=("discord" "spotify")
	for app in "${snapClassicList[@]}"
	do
		sudo snap install --classic $app
	done
	for app in "${snapList[@]}"
	do
		sudo snap install $app
	done
}

function _setupWeeklyBackupCron() {
	tempFile=$(mktemp)
	crontab -l > "$tempFile"
	while read line; do
		echo $line >> "$tempFile"
	done < $backupDir/backup-scripts/weekly-backup.crontab
	crontab "$tempFile"
}

# Check if script is running as root
if [ "$EUID" -eq 0 ]
  then echo "Please run this script as a non-root user"
  exit
else
	createSoftLinks
	copyConfigFilesFromBackup
	_installApplicationsWithAurHelper
	_installAppsWithSnap
	_setupWeeklyBackupCron
fi

