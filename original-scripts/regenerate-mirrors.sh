#! /bin/bash

declare -r COLOR_RED='\033[0;31m'
declare -r COLOR_GREEN='\033[0;32m'
declare -r COLOR_YELLOW='\033[1;33m'
declare -r COLOR_NONE='\033[0m'
declare -r tempMirrorFile="/etc/pacman.d/mirrorlist.temp"

function _pullNewMirrorListFromUpstream() {
    curl "https://www.archlinux.org/mirrorlist/?country=BR&country=CA&country=CL&country=CO&country=DK&country=EC&country=FI&country=FR&country=GE&country=DE&country=GR&country=HK&country=HU&country=IS&country=IN&country=ID&country=IE&country=IL&country=IT&country=JP&country=KZ&country=KE&country=LV&country=LT&country=LU&country=MK&country=MX&country=NL&country=NC&country=NZ&country=NO&country=PY&country=PH&country=PL&country=PT&country=RO&country=SG&country=SK&country=SI&country=ZA&country=KR&country=ES&country=SE&country=CH&country=TW&country=TH&country=TR&country=UA&country=GB&country=US&country=VN&protocol=https&ip_version=4&use_mirror_status=on" > "$tempMirrorFile"
    # Uncomment all the servers
    sed -i 's/^#Server/Server/' "$tempMirrorFile"
    # Remove all the comments from the files, they will just be a nuisance when we sort
    sed -i -e 's/#.*$//' -e '/^$/d' "$tempMirrorFile"
}

function _rankAndSortMirrors() {
    echo -e "\n${COLOR_YELLOW}Starting to sort all mirrors, this might take a bit...${COLOR_NONE}\n"
    rankmirrors "$tempMirrorFile" > /etc/pacman.d/mirrorlist
    echo -e "\n${COLOR_GREEN}All done!${COLOR_NONE}\n"
}

function _cleanupBackupMirrors() {
    if [ -f "$tempMirrorFile" ]; then
        rm -v "$tempMirrorFile"
        echo -e "\n${COLOR_GREEN}Deleting temporary mirror file.${COLOR_NONE}\n"
    fi
}

# Check if running Arch
if [ "$(awk 'NR==1 {print $1}' /etc/issue)" == "Arch" ]; then
    # Check if script is running as root
    if [ "$EUID" -ne 0 ]; then
        echo -e "\n${COLOR_RED}Please run this script as root${COLOR_NONE}\n"
        exit
    else
        _pullNewMirrorListFromUpstream
        if [ $(command -v rankmirrors) ] ; then
            _rankAndSortMirrors
        else
            echo -e "\n${COLOR_RED}rankmirrors command not found. Install pacman-contrib and try again.${COLOR_NONE}\n"
        fi
        _cleanupBackupMirrors
    fi
else
    echo -e "\n${COLOR_RED}Not running Arch Linux, aborting script.${COLOR_NONE}\n"
fi