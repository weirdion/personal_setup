#!/bin/bash

# profile.d/ script to define environment variables

declare -r username="$LOGNAME"
declare -r homeDir="/home/$username"
declare -r sdkDir="$homeDir/sdk"
declare -r sdkmanDir="$homeDir/.sdkman/candidates"

# export environment variables
export BROWSER=/usr/bin/firefox
export EDITOR=/usr/bin/vim
export ANDROID_HOME="$sdkDir/android-sdk"
export ANDROID_NDK_HOME="$ANDROID_HOME/ndk-bundle/"
export LD_LIBRARY_PATH=/usr/local/lib
export JAVA_HOME="$sdkmanDir/java/curent"
export M2_HOME="$sdkmanDir/maven/current"
export GRADLE_HOME="$sdkmanDir/gradle/current"
export GOPATH="$homeDir/go"
export FLUTTER_HOME="$sdkDir/flutter"
export RUBY_GEM_HOME="$homeDir/.gem/ruby/current"
export LOCAL_BIN="$homeDir/.local/bin"

# Append our default paths
# Taken from Arch Linux /etc/profile so this script can be used for all distros
appendpath () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            PATH="${PATH:+$PATH:}$1"
    esac
}

appendpath "$ANDROID_HOME/platform-tools"
appendpath "$FLUTTER_HOME/bin"
appendpath "$GOPATH/bin"
appendpath "$homeDir/bin"
appendpath "$RUBY_GEM_HOME/bin"
appendpath "$LOCAL_BIN"

unset appendpath

export PATH
