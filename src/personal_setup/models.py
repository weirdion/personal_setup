#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Models module contains the data structures and enums.
"""
from dataclasses import dataclass
from enum import auto

from personal_setup.utility_base import BaseEnum


class BackupCommandOptions(BaseEnum):
    """
    BackupCommandOptions class extends BaseEnum.
    This class contains all backup_option options used in cli for backup command.
    """

    ADD = auto()
    REMOVE = auto()
    LIST = auto()
    RUN = auto()


class BackupSubCommandOptions(BaseEnum):
    """
    BackupSubCommandOptions class extends BaseEnum.
    This class contains all sub-command options used in cli for backup command.
    """

    SYMLINK = auto()


class RestoreCommandOptions(BaseEnum):
    """
    RestoreCommandOptions class extends BaseEnum.
    This class contains all restore_option options used in cli for restore command.
    """
    ALL = auto()
    SYMLINK = auto()


@dataclass
class Symlink:
    """
    Symlink class is a dataclass that serves only to hold source and destination of the symbolic
    link.
    """
    source: str
    destination: str
