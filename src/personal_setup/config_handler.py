#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
config handler module handles generation and parsing of personal_setup.json
"""
import json
import os

from personal_setup import log, constants, path_handler, utility_base
from personal_setup.models import Symlink

KEY_SYMLINKS = 'symlinks'

_cli_logger = log.console_logger()
_logger = log.create_logger(__name__)


class PersonalSetupConfig:
    """
    PersonalSetupConfig class handles the structure and contents of the personal_setup.json config.
    """

    def __init__(self, symlinks=None):
        if symlinks is None:
            symlinks = list()

        self.symlinks = symlinks

    def is_symlink_in_list(self, symlink_target) -> (int, Symlink):
        """
        Helper method that checks the existing symlinks list to see if symlink_target exists.
        :param symlink_target: Path to the symlink target
        :return: If found, index and Symlink object is returned
        :return: If not found, -1 and None is returned
        """
        for index, symlink_obj in enumerate(self.symlinks):
            if symlink_obj.destination == symlink_target:
                return index, symlink_obj
        return -1, None


def read_config_from_file(file_name) -> PersonalSetupConfig:
    """
    Function that opens file_name in read only mode, parses/decodes json and returns as WiFiConfig.
    :param file_name: the full path of the file that contains the config
    :return: An object of L{PersonalSetupConfig}
    :exception: TypeError, raised if the config file is not of WiFiConfig type.
    :exception: FileNotFoundError, raised if the file_name provided doesn't exist.
    """
    personal_setup_config = None

    file_name = utility_base.get_path(file_name)
    try:
        with open(file_name, 'r') as config_file:
            try:
                config_dict = json.load(config_file)
                _logger.info(f'Reading config file: {file_name}.')
                personal_setup_config = _parse_personal_setup_config_dict(config_dict)
            except (TypeError, ValueError) as e:
                raise TypeError(f"File was not of type PersonalSetupConfig: {file_name}") from e
    except FileNotFoundError as err:
        raise FileNotFoundError(f"File not found: {file_name}") from err
    return personal_setup_config


def get_current_personal_setup_config(file_name=None) -> PersonalSetupConfig:
    """
    Function that attempts to retrieve existing personal setup config.
    If none exists or there is an error reading it, a new object of PersonalSetupConfig is returned.
    :param file_name: the full path of the file that contains the config
    :return: An object of L{PersonalSetupConfig}
    """
    personal_setup_config = None

    if not file_name:
        file_name = path_handler.get_config_file_path()

    try:
        personal_setup_config = read_config_from_file(file_name)
    except TypeError as err:
        err_msg = f'Parsing failed of the config file: {file_name}'
        _logger.error(f'{err_msg}. Error: {err}')
        _cli_logger.error(f'{err_msg}. Using empty configuration.')
    except FileNotFoundError as err:
        err_msg = f'No existing configuration detected at: {file_name}'
        _logger.warning(f'{err_msg}. Error: {err}')
        _cli_logger.warning(f'{err_msg}. Using empty configuration.')

    if not personal_setup_config:
        personal_setup_config = PersonalSetupConfig()
    return personal_setup_config


def write_config_to_file(personal_config: PersonalSetupConfig, file_name=None) -> bool:
    """
    Function that writes the personal_config to file.
    :param personal_config: PersonalSetupConfig, object that contains the config to be saved.
    :param file_name: str, absolute path to config file.
    :return: bool, True if the config was successfully saved, False otherwise.
    """
    file_written = False
    if not file_name:
        file_name = path_handler.get_config_file_path()

    file_name = utility_base.get_path(file_name)

    if os.path.isfile(file_name):
        backup_file_name = file_name + constants.CONFIG_BACKUP_SUFFIX
        _logger.info(f'Config file already exists, saving existing config to: {backup_file_name}.')
        os.replace(src=file_name, dst=backup_file_name)

    try:
        with open(file_name, 'w') as file:
            _logger.info(f'Writing config file: {file_name}.')
            file.write(utility_base.convert_to_json_str(personal_config, indent=4))
            file_written = True
    except IOError as err:
        _logger.error(f'Failed to write config file: {file_name}, reason: {err}')

    return file_written


# Internal helper functions

def _parse_personal_setup_config_dict(config_dict) -> PersonalSetupConfig:
    """
    Helper function that takes the config_dict received and converts it into PersonalSetupConfig.
    :param config_dict: dict, config dictionary.
    :return: PersonalSetupConfig, generated object from the config_dict.
    """
    personal_setup_config = PersonalSetupConfig()
    if KEY_SYMLINKS in config_dict:
        try:
            for item in config_dict[KEY_SYMLINKS]:
                if len(item) == 2:
                    personal_setup_config.symlinks.append(Symlink(item['source'],
                                                                  item['destination']))
                else:
                    _logger.warning(f"Invalid key format encountered, skipping {item}")
        except KeyError as err:
            _logger.error(f"Encountered error while parsing symlinks from config, {err}")

    return personal_setup_config
