#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
cli module handles the creation and use of the cli arguments
"""
from argparse import ArgumentParser, _HelpAction, _SubParsersAction
import sys

from personal_setup import command_handler, constants
from personal_setup.models import BackupCommandOptions, BackupSubCommandOptions, \
    RestoreCommandOptions


class _HelpActionLongFormat(_HelpAction):  # pylint: disable=too-few-public-methods
    """
    Overridden class of argparse._HelpAction to allow long format print of help.
    """

    def __call__(self, parser, namespace, values, option_string=None):
        parser.print_help()

        # retrieve subparsers from parser
        subparsers_actions = [
            action for action in parser._actions
            if isinstance(action, _SubParsersAction)]

        for subparsers_action in subparsers_actions:
            # get all subparsers and print help
            for choice, subparser in subparsers_action.choices.items():
                print(f"\nCommand '{choice}'")
                print(subparser.format_help())

        parser.exit()


def create_parser() -> ArgumentParser:
    """
    Function that creates the argument parser.
    :return: ArgumentParser, the object of ArgumentParser with configured options.
    """
    parser = ArgumentParser(
        prog=constants.CLI_NAME,
        description='A cli tool to backup and help re-install system.',
        add_help=False
    )

    parser.add_argument(
        '-h',
        '--help',
        action=_HelpActionLongFormat
    )

    parser.add_argument(
        '-v',
        '--version',
        action='version',
        version=f"{constants.CLI_NAME} {constants.__version__}"
    )

    sub_parsers = parser.add_subparsers(
        title='Commands',
        description=f'Commands available in {constants.CLI_NAME}:',
        dest='command'
    )

    backup_parser = sub_parsers.add_parser(
        name='backup',
        help='Handles all options related to backing up existing user items. '
    )
    _backup_parser_options(backup_parser)

    restore_parser = sub_parsers.add_parser(
        name='restore',
        help='Handles all options related to restoring backed-up items.'
    )
    _restore_parser_options(restore_parser)

    return parser


def _backup_parser_options(backup_parser):
    """
    Function that adds the available sub-commands to 'backup' command.
    """
    backup_subparser = backup_parser.add_subparsers(
        title='backup options',
        description=f'Options available for configuring/running backup',
        dest='backup_option',
    )
    backup_subparser.required = True

    # Backup AddItem types that can be added to the backup list
    backup_add_parser = backup_subparser.add_parser(
        BackupCommandOptions.ADD.value,
        help=f'Adds items to the backup list.'
    )

    backup_add_sub_parser = backup_add_parser.add_subparsers(
        title='backup add options',
        required=True,
        description='Item types that can be added to the backup list',
        dest='add_option'
    )
    backup_add_sub_parser.required = True

    backup_add_symlink_parser = backup_add_sub_parser.add_parser(
        BackupSubCommandOptions.SYMLINK.value,
        help="Symbolic link that needs to back back'ed up"
    )
    backup_add_symlink_parser.add_argument(
        'symlink_object',
        help='The current symbolic link that needs to be backed up',
    )

    # Backup Remove
    backup_remove_parser = backup_subparser.add_parser(
        name=BackupCommandOptions.REMOVE.value,
        help=f'Removes items from the backup list.'
    )
    backup_remove_sub_parser = backup_remove_parser.add_subparsers(
        title='backup remove options',
        required=True,
        description='Item types that should be removed from the backup list',
        dest='remove_option'
    )
    backup_remove_sub_parser.required = True

    backup_remove_sub_parser.add_parser(
        BackupSubCommandOptions.SYMLINK.value,
        help="The symbolic link that no longer needs to be back'ed up"
    )

    # Backup List
    backup_list_parser = backup_subparser.add_parser(
        name=BackupCommandOptions.LIST.value,
        help=f'Prints out the list of backup items.'
    )
    backup_list_sub_parser = backup_list_parser.add_subparsers(
        title='backup list options',
        description='List a subset based on the option specified',
        dest='list_option'
    )
    backup_list_sub_parser.add_parser(
        BackupSubCommandOptions.SYMLINK.value,
        help="List all the symbolic links present in the backup list"
    )

    # Backup Run
    backup_run_parser = backup_subparser.add_parser(
        name=BackupCommandOptions.RUN.value,
        help=f'Runs backup for all items currently in the list.'
    )
    backup_run_sub_parser = backup_run_parser.add_subparsers(
        title='backup run options',
        description='run backup on a subset based on the option specified',
        dest='run_option'
    )
    backup_run_sub_parser.add_parser(
        BackupSubCommandOptions.SYMLINK.value,
        help="Run a backup of all symbolic links present in the backup list"
    )


def _restore_parser_options(restore_parser):
    """
    Function that adds the available sub-commands to 'restore' command.
    """
    restore_parser.add_argument(
        'restore_option',
        choices=[
            RestoreCommandOptions.ALL.value,
            RestoreCommandOptions.SYMLINK.value
        ],
        help="Selectively restore all or sub-set of the back'ed up items"
    )


def main():
    """
    Function that serves as the primary entry-point for the cli.
    """
    if len(sys.argv) <= 1:
        raise SystemExit(
            f"{constants.CLI_NAME}: No command received. "
            f"Run `{constants.CLI_NAME} --help` to see available options.")

    args = create_parser().parse_args()
    command_handler.parse_received_command(args)
