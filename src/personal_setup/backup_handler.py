#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
backup handler module handles all operations related to backup command.
"""
import logging
import os

from personal_setup import log, config_handler, utility_base, path_handler
from personal_setup.models import BackupSubCommandOptions, Symlink

_cli_logger = log.console_logger()
_logger = log.create_logger(__name__)


def add_item(received_command):
    """
    Function that handles adding the item to the backup list.
    :param received_command: Namespace object received from parsed arguments from the cli.
    """
    if received_command.add_option == BackupSubCommandOptions.SYMLINK.value:
        _add_item_symlink(received_command.symlink_object)


def remove_item(received_command):
    """
    Function that handles removing the item to the backup list.
    :param received_command: Namespace object received from parsed arguments from the cli.
    """
    if received_command.remove_option == BackupSubCommandOptions.SYMLINK.value:
        _remove_item_symlink()


def list_items(list_option):
    """
    Function that handles displaying the entire or sub-set of the backup list.
    :param list_option: sub-set of the backup list to be displayed.
    """
    if list_option == BackupSubCommandOptions.SYMLINK.value:
        _cli_logger.info('Listing symlinks currently in backup list:')
        _list_symlinks()
    else:
        _cli_logger.info('Listing all items in the backup list:')
        _list_all()


def run_backup(run_option):
    """
    Function that handles running the entire or sub-set of the backup list.
    :param run_option: sub-set of the backup list to be run.
    """


# Helper functions

def _add_item_symlink(symlink_object):
    """
    Inner helper function that handles adding an symlink item to the backup list.
    This fails and prints error message if the object is invalid or not a symbolic link.
    """
    # Change symlink_object received to absolute path without following down symlink
    symlink_object = os.path.abspath(symlink_object)

    if os.path.islink(symlink_object):
        config_file_name = path_handler.get_config_file_path()
        personal_setup_config = config_handler.get_current_personal_setup_config(config_file_name)
        index, existing_symlink = personal_setup_config.is_symlink_in_list(symlink_object)

        add_to_list = True

        if index != -1:
            _cli_logger.info(f'Conflict found in existing backup list,\n'
                             f'source: {existing_symlink.source}\n'
                             f'destination: {existing_symlink.destination}')
            add_to_list = utility_base.prompt_user_yes_no('Do you want to replace it?')

        if add_to_list:
            # Follow down the symlink to real-path
            source = os.path.realpath(symlink_object)
            new_symlink = Symlink(source, symlink_object)

            if index != -1:
                personal_setup_config.symlinks[index] = new_symlink
            else:
                personal_setup_config.symlinks.append(new_symlink)
            write_result = config_handler.write_config_to_file(personal_setup_config,
                                                               config_file_name)
            if write_result:
                log.combined_log(_logger, _cli_logger, logging.INFO,
                                 f'Successfully updated the backup list.')
            else:
                _cli_logger.error(f'Failed to update the backup list, please see logs for details.')
        else:
            log.combined_log(_logger, _cli_logger, logging.INFO,
                             'Not adding the symlink to the backup list.')
    else:
        log.combined_log(_logger, _cli_logger, logging.INFO,
                         'The file path given is not a valid symbolic link.')


def _remove_item_symlink():
    """
    Inner helper function that prints available symlinks in backup list then prompts the user to
    delete any of them.
    """
    config_file_name = path_handler.get_config_file_path()

    while True:
        personal_setup_config = config_handler.get_current_personal_setup_config(config_file_name)
        if not personal_setup_config.symlinks:
            _cli_logger.info('\nNo symlinks available to remove in the backup list.')
            break

        _cli_logger.info('\nAvailable symlinks in backup list:')
        _list_symlinks_from_config(personal_setup_config)
        question = 'Please enter the symlink index to remove (or enter 0 to exit): '
        valid_inputs = [str(n) for n in range(0, len(personal_setup_config.symlinks) + 1)]
        while (answer := input(question)) not in valid_inputs:
            print(f"Invalid input: {answer}.")

        if answer == '0':
            break

        index = int(answer) - 1
        symlink = personal_setup_config.symlinks[index]
        _cli_logger.info('\nSelected symlink:\n'
                         f'source: {symlink.source}\n'
                         f'destination: {symlink.destination}')
        response_yes_no = utility_base.prompt_user_yes_no('Do you want to remove it?')
        if response_yes_no:
            personal_setup_config.symlinks.pop(index)
            config_handler.write_config_to_file(personal_setup_config, config_file_name)

    _cli_logger.info('Exiting remove mode.')


def _list_symlinks():
    """
    Inner helper function that lists all symlinks in the backup list.
    """
    config_file_name = path_handler.get_config_file_path()
    personal_setup_config = config_handler.get_current_personal_setup_config(config_file_name)
    _list_symlinks_from_config(personal_setup_config)


def _list_symlinks_from_config(personal_setup_config: config_handler.PersonalSetupConfig):
    for index, symlink in enumerate(personal_setup_config.symlinks):
        _cli_logger.info(f'{index + 1}: source: {symlink.source}\n'
                         f'   destination: {symlink.destination}')


def _list_all():
    """
    Inner helper function that lists all items in the backup list.
    """
    config_file_name = path_handler.get_config_file_path()
    # Load config once to avoid repeat reads from the filesystem
    personal_setup_config = config_handler.get_current_personal_setup_config(config_file_name)

    _cli_logger.info('Symlinks:')
    _list_symlinks_from_config(personal_setup_config)
