#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
utility system module contains helper functions that interact with the system.
"""

import os

from personal_setup import log

_logger = log.create_logger(__name__)
_cli_logger = log.console_logger()


def create_symlink(source, destination):
    """
    Function that creates a symlink from source to destination.
    :param source: source file or directory that needs to be linked.
    :param destination: destination file or directory where the link will be made.
    """
    try:
        os.symlink(source, destination)
        symlink_success = True
    except OSError as err:
        err_msg = f'Failed to create symlink from src: {source} dest: {destination}.'
        _cli_logger.error(err_msg)
        _logger.error(
            f'{err_msg} Reason: {err}')
        symlink_success = False

    return symlink_success
