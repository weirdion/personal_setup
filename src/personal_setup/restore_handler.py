#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
restore handler module handles all operations related to restore command.
"""
import logging
import os

from personal_setup import config_handler, log, path_handler, utility_base, utility_system

_cli_logger = log.console_logger()
_logger = log.create_logger(__name__)


def restore_symlinks():
    """
    Function that handles restoring the sub-set of the backup list: symlinks.
    """
    personal_setup_config = _check_and_load_personal_setup_config()
    if personal_setup_config:
        if not personal_setup_config.symlinks:
            _cli_logger.info(f"No Symlinks found in the backup list, nothing to restore.")
        else:
            log.combined_log(_logger, _cli_logger, logging.INFO, 'Starting to restore symlinks...')
            _restore_symlinks_list(personal_setup_config.symlinks)


def restore_all():
    """
    Function that handles restoring the entire of the backup list.
    """
    personal_setup_config = _check_and_load_personal_setup_config()
    if personal_setup_config:
        if not personal_setup_config.symlinks:
            _cli_logger.info(f"No Symlinks found in the backup list.")
        else:
            log.combined_log(_logger, _cli_logger, logging.INFO, 'Starting to restore symlinks...')
            _restore_symlinks_list(personal_setup_config.symlinks)


def _check_and_load_personal_setup_config():
    """
    Inner helper function that checks if the backup config list exists and loads it.
    :return None, if the file doesn't exist
    :return PersonalSetupConfig, if the file exists
    """
    config_file_name = path_handler.get_config_file_path()

    if not os.path.isfile(config_file_name):
        _cli_logger.info(f"Backup list: {config_file_name} does not exist, nothing to restore.")
        return None

    _cli_logger.info(f"Loading backup list: {config_file_name}.")
    return config_handler.get_current_personal_setup_config(config_file_name)


def _restore_symlinks_list(symlinks_list: list):
    """
    Inner helper function that iterates overt eh symlink_list and tries to restore the symlink.
    """
    for symlink in symlinks_list:
        log.combined_log(_logger, _cli_logger, logging.INFO,
                         f"\nRestoring symlink, src: {symlink.source}  "
                         f"dest: {symlink.destination}")
        _restore_individual_symlink(symlink)


def _restore_individual_symlink(symlink):
    """
    Inner-helper function that checks source and destination to validate source and destination,
    then calls utility_system.create_symlink.
    :param symlink: models.Symlink object to restore.
    """
    create_symlink_bool = True
    if os.path.lexists(symlink.destination):
        create_symlink_bool = False
        if os.path.islink(symlink.destination):
            real_path = os.path.realpath(symlink.destination)
            if symlink.source == real_path:
                _cli_logger.warning('Symlink is already correctly set up, skipping.')
            else:
                _cli_logger.warning(f'Symlink destination already exists, \n'
                                    f'expected source: {symlink.source} found source: {real_path}')
                answer = utility_base.prompt_user_yes_no('Do you want to replace it?')
                if answer:
                    try:
                        _logger.warning(f'Removing existing symlink destination: '
                                        f'{symlink.destination}')
                        os.remove(symlink.destination)  # for python os.remove is same as os.unlink
                        create_symlink_bool = True
                    except OSError as err:
                        _cli_logger.error('Failed to remove the symlink destination, skipping.')
                        _logger.error(f'Failed to remove the symlink destination for {symlink}.'
                                      f'Reason: {err}')
        else:
            _cli_logger.warning("Warning: File exists by the same name at destination: "
                                f"{symlink.destination}.")
            answer = utility_base.prompt_user_yes_no('Do you want to remove it?')
            if answer:
                try:
                    _logger.warning(f'Removing existing symlink destination file: '
                                    f'{symlink.destination}')
                    os.remove(symlink.destination)  # for python os.remove is same as os.unlink
                    create_symlink_bool = True
                except OSError as err:
                    _cli_logger.error('Failed to remove the symlink destination file, skipping.')
                    _logger.error(f'Failed to remove the symlink destination file for {symlink}.'
                                  f'Reason: {err}')

    if create_symlink_bool:
        utility_system.create_symlink(symlink.source, symlink.destination)
