#  personal_setup, a cli tool to backup and help re-install system.
#  Copyright (C) 2020  Ankit Sadana
#
#  personal_setup is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  personal_setup is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
command handler module handles operations after the cli receives a command
"""
import logging

from personal_setup import backup_handler, constants, log, restore_handler
from personal_setup.models import BackupCommandOptions, RestoreCommandOptions

_cli_logger = log.console_logger()
_logger = log.create_logger(__name__)


def parse_received_command(received_args):
    """
    Function that parses the received args into commands and executes the respective function.
    :params received_args: Namespace object received from ArgumentParser use.
    """
    log.combined_log(_logger, _cli_logger, logging.DEBUG, f'Command received: {received_args}')
    if received_args.command == constants.CLI_COMMAND_BACKUP:
        _received_command_backup(received_args)
    elif received_args.command == constants.CLI_COMMAND_RESTORE:
        _received_command_restore(received_args)


def _received_command_backup(received_args):
    """
    Function that handles backup command options.
    :params received_args: Namespace object received from ArgumentParser use.
    """
    if received_args.backup_option == BackupCommandOptions.ADD.value:
        backup_handler.add_item(received_args)
    elif received_args.backup_option == BackupCommandOptions.REMOVE.value:
        backup_handler.remove_item(received_args)
    elif received_args.backup_option == BackupCommandOptions.LIST.value:
        backup_handler.list_items(received_args.list_option)
    elif received_args.backup_option == BackupCommandOptions.RUN.value:
        backup_handler.run_backup(received_args.run_option)


def _received_command_restore(received_args):
    """
    Function that handles restore command options.
    :params received_args: Namespace object received from ArgumentParser use.
    """
    if received_args.restore_option == RestoreCommandOptions.SYMLINK.value:
        restore_handler.restore_symlinks()
    elif received_args.restore_option == RestoreCommandOptions.ALL.value:
        restore_handler.restore_all()
