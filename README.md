# personal_setup ![Pipeline Status][pipeline_image] ![Coverage][coverage_image]


Python CLI program to help simplify and automate backup and re-initialization of Arch Linux or Ubuntu Linux.

## Features

TODO

## Dependencies

1. [jq] - Needed for parsing Pipfile.lock and updating requirements.txt (development only).

## Preparing for Development

1. Ensure ``pip`` and ``pipenv`` are installed
2. Clone repository
3. ``cd`` into repository
4. Fetch development dependencies ``make``
5. Initiate a local install for virtual system directories ``make install-pipenv``
6. Activate virtualenv: ``pipenv shell``

## Usage

personal_setup has TODO


To view the help page, and all available options,

```sh
    $ personal_setup --help
```

## Running Tests

Run tests:

```sh
    $ make test
```

[jq]: <https://stedolan.github.io/jq/>
[pipeline_image]: <https://gitlab.com/asadana/personal-setup/badges/develop/pipeline.svg>
[coverage_image]: <https://gitlab.com/asadana/personal-setup/badges/develop/coverage.svg>
